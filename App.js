/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
// TODO 2回リロードしないと起動しない現象が起きるので要確認
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Picker,
} from 'react-native'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { StackNavigator, addNavigationHelpers } from 'react-navigation';

import Main from './app/components/Main'
import ProfileForm from './app/components/ProfileForm'
import ProfileList from './app/containers/ProfileList'
import reducer from './app/reducer'

const AppNavigator = StackNavigator({
  Main: {screen: Main},
  ProfForm: {screen: ProfileForm},
  ProfList: {screen: ProfileList}
});

const store = createStore(reducer)
export default class App extends Component<Props> {
  constructor(props) {
    super(props)
  }

  // Providerから複数のcomponentは呼べない
  render() {
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}
