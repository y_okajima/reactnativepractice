# README #

ReactNative sample app

## run

`$ npm install`

`$ react-native run-ios`


### Downloading Javascript Bundleが完了しても起動しない場合(debug時のみ？)
ReactNative現行バージョンのバグのようです。
[[0.54] Live Reload is broken on DeltaPatcher.js error #18209](https://github.com/facebook/react-native/issues/18209)
リロードしていただければ表示されるかと思います。