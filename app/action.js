export const ADD_PROFILE = 'ADD_PROFILE'

export function addProfile(prof) {
  return { 
    type: ADD_PROFILE, 
    prof 
  }
}
