import { combineReducers } from 'redux'
import { ADD_PROFILE } from './action'

const profiles = (state = [], action) => {
  switch (action.type) {
    case ADD_PROFILE:
      return [
        ...state,
        {
          name: action.prof.name,
          gender: action.prof.gender,
          area: action.prof.area
        }
      ]
    default:
      return state
  }
}

const profileApp = combineReducers({
  profiles
})

export default profileApp
