import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
    flexDirection: 'row', // 横並びになる
    alignItems: 'center',
    justifyContent: 'center',
  },
  formItem1: {
    flex: 1,
    padding: 20
  },
  formItem2: {
    flex: 1,
    padding: 20
  },
  formItem3: {
    flex: 1,
    padding: 20
  },
  flexEnd: {
    alignItems: "flex-end",
    padding: 10
  }
});