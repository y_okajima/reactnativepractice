import { connect } from 'react-redux'
import ProfList from '../components/ProfList'

// storeからstateを取得しpropsとして渡す
const mapStateToProps = state => {
  return {
    profiles: state.profiles
  }
}
// connectはcomponentsとは分離するべき？
// dispatch処理をラップする
const mapDispatchToProps = dispatch => {
  return {
    addProfile: prof => {
      dispatch(addProfile(prof))
    }
  }
}

const ProfileList = connect(mapStateToProps, mapDispatchToProps)(ProfList)
export default ProfileList

