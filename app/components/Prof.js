import React, { Component, } from 'react'
import { View, Text } from 'react-native'

class Prof extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View>
        <Text>お名前:{this.props.name}</Text>
        <Text>性別:{this.props.gender}</Text>
        <Text>地域:{this.props.area}</Text>
      </View>
    )
  }
}

export default Prof