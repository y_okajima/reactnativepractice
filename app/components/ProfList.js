import React, { Component, } from 'react'
import { View, Text } from 'react-native'
import Swiper from 'react-native-swiper';
import { styles } from '../styles'
import Prof from './Prof'

class Profile extends Component {
  static navigationOptions = {
    title: "一覧"
  }
  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
  }

  render() {
    return (
      // swiper表示
      // listの中身を一つずつ分解してprofを描画
      <Swiper>
        {this.props.profiles.map((profile) =>
          <View style={styles.container}>
            <Prof {...profile}/>
          </View>
        )}
      </Swiper>
    )
  }
}

export default Profile