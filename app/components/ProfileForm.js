import React, { Component, } from 'react'
//import {DatePicker} from 'react-native-ui-xg'
import { connect } from 'react-redux'
import {
  View,
  Text,
  Picker,
  TextInput,
  Button,
} from 'react-native'
import { addProfile } from '../action'
import { styles } from '../styles'

const areaValue = [
    '北海道',
    '東北', 
    '関東', 
    '中部',
    '近畿', 
    '中国',
    '四国',
    '九州',
]

class ProfileForm extends Component {
  static navigationOptions = {
    title: "追加"
  }
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      gender: '男',
      area: '関東'
    }
  }
  
  // アロー関数の方が良さそう
  //handleChange(text) {
  //  this.setState({name:text})
  //}
  componentDidMount() {
  }
  
  saveProfile() {
    //this.props.dispatch(addProfile(this.state)
    // 直接stateを渡してもよいのか
    this.props.addProfile(this.state)
  }
  
  render() {
    return (
      <View style={styles.formContainer}>
        <View style={styles.formItem1}>
          <Text>名前</Text>
          <TextInput placeholder='名前を入力してください' value={this.state.name} 
            onChangeText={(text) => {this.setState({name:text})}}/>
        </View>
        <View style={styles.formItem2}>
          <Text>性別</Text>
          <Picker 
            style={{width: 100,height: 10}}
            textStyle={{fontSize: 3}}
            selectedValue={(this.state && this.state.gender) || '男'}
            onValueChange={(value) => {
              this.setState({gender:value})
            }}>
            <Picker.Item label={'男'} value={'男'} />
            <Picker.Item label={'女'} value={'女'} />
            <Picker.Item label={'その他'} value={'その他'} />
          </Picker>
        </View>
        <View style={styles.formItem3}>
          <Text>お住いの地域</Text>
          <Picker 
            style={{width: 100,height: 10}}
            textStyle={{fontSize: 3}}
            selectedValue={(this.state && this.state.area) || '関東'}
            onValueChange={(value) => {
              this.setState({area:value})
            }}>
            {areaValue.map(area =>
              <Picker.Item label={area} value={area} />            
            )}
          </Picker>
          <View style={styles.flexEnd}>
            <Button
              onPress={() => this.saveProfile()}
              title="保存"
              color="#841584"
            />
          </View>
        </View>
      </View>
    )
  }
}

// connectはcomponentsとは分離するべき？
// dispatch処理をラップする
const mapDispatchToProps = dispatch => {
  return {
    addProfile: prof => {
      dispatch(addProfile(prof))
    }
  }
}

// mapStateToProps, mapDispatchToProps, mergeProps
// mapStateToProps -> storeからstateを取り出しpropsに渡す
// mapDispatchToProps -> dispatchメソッドを受け取りactionを発行する
// mergeProps -> 前者の引数をマージし指定したcomponentにpropsを渡す
// 必要なのはdispatchのみなので第一は無し
export default connect(() => { return {} }, mapDispatchToProps)(ProfileForm)