import React, { Component, } from 'react'
import { View, Button } from 'react-native'
import Orientation from 'react-native-orientation'
import ProfileForm from './ProfileForm'
import ProfileList from '../containers/ProfileList' // componentsではなくcontainerを渡す
import { styles } from '../styles'

class Main extends Component {
  static navigationOptions = { title: "ホーム" }
  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
  }
  // WillAmountはdeprecated
  componentDidMount() {
    Orientation.lockToLandscape();
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Button title="プロフィール追加" onPress={ () => navigate('ProfForm') } />
        <Button title="プロフィール一覧" onPress={ () => navigate('ProfList') } />
      </View>
    )
  }
}

export default Main